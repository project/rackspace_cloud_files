<?php

namespace Drupal\rackspace_cloud_files;

use OpenCloud\Rackspace;


class RackSpaceCloudFilesService
{

  /**
   * @param $containerName
   * @return \OpenCloud\ObjectStore\Resource\Container
   */
  public function getContainer($containerName)
  {
    return $this->getObjectStoreService()->getContainer($containerName);
  }

  /**
   * @return \OpenCloud\ObjectStore\Service
   */
  public function getObjectStoreService()
  {
    return $this->cloudFilesCredentials()->objectStoreService(null, 'IAD');
  }

  /**
   * @param $fromContainer
   * @param $toContainer
   */
  public function copyToContainer($fromContainer, $toContainer)
  {
    $objects = $this->getContainer($fromContainer)->objectList();
    foreach ($objects as $object) {
      $file = $object->getPublicUrl()->getPath();
      if ($file[0] == "/") {
        $uploadFile = substr($file, 1);
      } else {
        $uploadFile = $file;
      }
      $this->copyFile($fromContainer, $uploadFile, $toContainer);
    }
  }

  /**
   * @param $containerName
   * @param $objectName
   * @param $destinationContainer
   */
  public function copyFile($containerName, $objectName, $destinationContainer)
  {
    $object = $this->getContainer($containerName)->getObject($objectName);
    $object->copy("$destinationContainer/$objectName");
  }

  /**
   * @param $localFileName
   * @param $directory
   * @param $containerName
   * @return \OpenCloud\ObjectStore\Resource\DataObject
   */
  public function uploadFile($localFileName, $directory, $containerName)
  {
    $container = $this->getContainer($containerName);
    $handle = fopen($localFileName, 'r');
    $result = $container->uploadObject($directory, $handle);
    fclose($handle);
    return $result;
  }

  /**
   * @param $containerName
   * @return array
   */
  public function getContainerMetaData($containerName)
  {
    return $this->getContainer($containerName)->getMetadata()->toArray();
  }

  /**
   * @param $containerName
   * @return array
   */
  public function getAllFiles($containerName)
  {
    $container = \Drupal::service('rackspace_cloud_files.default')->getContainer($containerName);
    $objects = $container->objectList();
    $urlArray = [];
    foreach ($objects as $object) {
      $url = $object->getPublicUrl()->getHost() . $object->getPublicUrl()->getPath();
      // Checking if has extension
      if (substr($url, -4, 1) === '.'){
        $urlArray[] = $url;
      }
    }
    return $urlArray;
  }


  public function downloadFiles($containerName)
  {
    $container = \Drupal::service('rackspace_cloud_files.default')->getContainer($containerName);
    $objects = $container->objectList();
    $urlArray = [];
    foreach ($objects as $object) {
      $url = $object->getPublicUrl()->getHost() . $object->getPublicUrl()->getPath();
      // Checking if have
      if (substr($url, -4, 1) === '.'){
        $urlArray[] = $url;
      }
    }
    return $urlArray;
  }



  /**
   * @return Rackspace
   */
  private function cloudFilesCredentials()
  {
    $config = \Drupal::config('rackspace_cloud_files.settings');
    return new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
      'username' => $config->get('rackspace_cloud_files.username'),
      'apiKey' => $config->get('rackspace_cloud_files.api_key'),
    ));
  }

}





