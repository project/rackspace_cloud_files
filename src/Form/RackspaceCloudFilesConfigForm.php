<?php
/**
 * @file
 * Contains \Drupal\rackspace_cloud_files\Form\RackspaceCloudFilesConfigForm.
 */

namespace Drupal\rackspace_cloud_files\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class RackspaceCloudFilesConfigForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'rackspace_cloud_files_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('rackspace_cloud_files.settings');
    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('rackspace_cloud_files.username'),
      '#required' => TRUE,
    );
    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Api Key'),
      '#default_value' => $config->get('rackspace_cloud_files.api_key'),
      '#required' => TRUE,
    );
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->config('rackspace_cloud_files.settings');
    $config->set('rackspace_cloud_files.username', $form_state->getValue('username'));
    $config->set('rackspace_cloud_files.api_key', $form_state->getValue('api_key'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'rackspace_cloud_files.settings',
    ];
  }

}
